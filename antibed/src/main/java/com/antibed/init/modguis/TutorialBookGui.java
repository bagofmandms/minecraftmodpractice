package com.antibed.init.modguis;

import com.antibed.util.Reference;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ResourceLocation;

import java.io.IOException;

public class TutorialBookGui extends GuiScreen {

    final ResourceLocation texture = new ResourceLocation(Reference.MOD_ID, "textures/gui/tutorial_book_texture.png");
    int guiHeight = 163;
    int guiWidth =  242;

    @Override
    public void drawScreen(int p_drawScreen_1_, int p_drawScreen_2_, float p_drawScreen_3_) {
        drawDefaultBackground();
        Minecraft.getMinecraft().renderEngine.bindTexture(texture);
        int centerX = (width / 2) - guiWidth / 2;
        int centerY = (height / 2 - guiHeight / 2);
        drawTexturedModalRect(centerX, centerY, 0, 0,  guiWidth, guiHeight);
        super.drawScreen(p_drawScreen_1_, p_drawScreen_2_, p_drawScreen_3_);
    }

    @Override
    public void initGui() {
        super.initGui();
    }

    @Override
    protected void actionPerformed(GuiButton p_actionPerformed_1_) throws IOException {
        super.actionPerformed(p_actionPerformed_1_);
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }
}