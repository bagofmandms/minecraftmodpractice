package com.antibed.util.handlers;

import com.antibed.util.Reference;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.storage.loot.LootTableList;

public class LootTableHandler
{
    public static final ResourceLocation FLAMY = LootTableList.register(new ResourceLocation(Reference.MOD_ID, "flamy"));
    public static final ResourceLocation PEBBLE = LootTableList.register(new ResourceLocation(Reference.MOD_ID, "pebble"));
}