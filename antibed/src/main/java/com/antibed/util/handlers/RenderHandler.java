package com.antibed.util.handlers;

import com.antibed.entities.customrenderers.RenderAnimeGirl;
import com.antibed.entities.flamy.EntityFlamy;
import com.antibed.entities.flamy.RenderFlamy;
import com.antibed.entities.modifiedentities.EntityAnimeGirl;
import com.antibed.entities.pebble.EntityPebble;
import com.antibed.init.ModFluids;
import com.antibed.entities.pebble.RenderPebble;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.fml.client.registry.IRenderFactory;
import net.minecraftforge.fml.client.registry.RenderingRegistry;

public class RenderHandler {
    public static void registerEntityRenders() {
        RenderingRegistry.registerEntityRenderingHandler(EntityFlamy.class, new IRenderFactory<EntityFlamy>() {
            @Override
            public Render<? super EntityFlamy> createRenderFor(RenderManager manager) {
                return new RenderFlamy(manager);
            }
        });
        RenderingRegistry.registerEntityRenderingHandler(EntityPebble.class, new IRenderFactory<EntityPebble>() {
            @Override
            public Render<? super EntityPebble> createRenderFor(RenderManager manager) {
                return new RenderPebble(manager);
            }
        });
        RenderingRegistry.registerEntityRenderingHandler(EntityAnimeGirl.class, new IRenderFactory<EntityAnimeGirl>() {
            @Override
            public Render<? super EntityAnimeGirl> createRenderFor(RenderManager manager) {
                return new RenderAnimeGirl(manager);
            }
        });
    }

    public void registerCustomMeshesAndStates() {
        ModFluids.renderFluids();
    }
}
