package com.antibed.entities.custommodels;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

public class ModelAnimeGirl extends ModelBase {
    public ModelRenderer Right_Arm;
    public ModelRenderer Right_Leg;
    public ModelRenderer Head;
    public ModelRenderer Torso;
    public ModelRenderer Left_Arm;
    public ModelRenderer Left_Leg;

    public ModelAnimeGirl() {
        this.textureWidth = 64;
        this.textureHeight = 64;
        this.Torso = new ModelRenderer(this, 16, 16);
        this.Torso.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Torso.addBox(-4.0F, 0.0F, -2.0F, 8, 12, 4, 0.0F);
        this.Left_Leg = new ModelRenderer(this, 16, 48);
        this.Left_Leg.setRotationPoint(1.9F, 12.0F, 0.0F);
        this.Left_Leg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, 0.0F);
        this.Left_Arm = new ModelRenderer(this, 32, 48);
        this.Left_Arm.setRotationPoint(5.0F, 2.0F, 0.0F);
        this.Left_Arm.addBox(-1.0F, -2.0F, -2.0F, 4, 12, 4, 0.0F);
        this.Head = new ModelRenderer(this, 0, 0);
        this.Head.setRotationPoint(0.0F, 0.0F, 0.0F);
        this.Head.addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, 0.0F);
        this.Right_Arm = new ModelRenderer(this, 32, 48);
        this.Right_Arm.setRotationPoint(-5.0F, 2.0F, 0.0F);
        this.Right_Arm.addBox(-3.0F, -2.0F, -2.0F, 4, 12, 4, 0.0F);
        this.Right_Leg = new ModelRenderer(this, 0, 16);
        this.Right_Leg.setRotationPoint(-1.9F, 12.0F, 0.0F);
        this.Right_Leg.addBox(-2.0F, 0.0F, -2.0F, 4, 12, 4, 0.0F);
    }

    @Override
    public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        this.Torso.render(f5);
        this.Left_Leg.render(f5);
        this.Left_Arm.render(f5);
        this.Head.render(f5);
        this.Right_Arm.render(f5);
        this.Right_Leg.render(f5);
    }

    /**
     * This is a helper function from Tabula to set the rotation of model parts
     */
    public void setRotateAngle(ModelRenderer modelRenderer, float x, float y, float z) {
        modelRenderer.rotateAngleX = x;
        modelRenderer.rotateAngleY = y;
        modelRenderer.rotateAngleZ = z;
    }

    @Override
    public void setRotationAngles(float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor, Entity entityIn) {
        this.Left_Leg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 0.9F * limbSwingAmount;
        this.Right_Leg.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float)Math.PI) * 0.9F * limbSwingAmount;

        this.Left_Arm.rotateAngleX = MathHelper.cos(limbSwing * 0.6F) * 0.9F * MathHelper.abs(limbSwingAmount);
        this.Right_Arm.rotateAngleX = MathHelper.cos(limbSwing * 0.6F + (float)Math.PI) * 0.9F * MathHelper.abs(limbSwingAmount);

        this.Head.rotateAngleY = netHeadYaw * 0.017453292F;
        this.Head.rotateAngleX = headPitch * 0.017453292F;
    }

}
