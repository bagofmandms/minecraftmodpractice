package com.antibed.entities;

import net.minecraft.entity.EntityLiving;
import net.minecraft.world.World;

public class EntityBase extends EntityLiving {

    private float eyeHeight;

    public EntityBase(World world, Float width, Float height, Float eyeHeight) {
        super(world);
        this.setSize(width, height);
        this.eyeHeight = eyeHeight;
    }

    @Override
    public float getEyeHeight() {
        return this.eyeHeight;
    }
}
